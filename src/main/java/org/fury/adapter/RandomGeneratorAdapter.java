package org.fury.adapter;

public class RandomGeneratorAdapter extends RandomGenerator implements RandomAdapter {
    @Override
    public int[] generate(int length) {
        int[] arr = new int[length];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = getRandomNumber();
        }
        return arr;
    }
}
