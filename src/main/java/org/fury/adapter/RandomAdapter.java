package org.fury.adapter;

public interface RandomAdapter {
    int[] generate(int length);
}
