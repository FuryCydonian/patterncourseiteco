package org.fury.strategy;

public interface SortStrategy {
    void sort(int[] array, int left, int right);
}
