package org.fury.strategy;

public class SortingService {

    public int[] sortArray(int[] array) {
        SortStrategy sortStrategy = array.length < 100 ? new BubbleSort() : new MergeSort();
        Sorter sorter = new Sorter(sortStrategy);
        return sorter.sort(array);
    }
}
