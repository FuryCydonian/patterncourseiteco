package org.fury.abstractFactory;

public class ModernBuildFactory implements BuildFactory {

    @Override
    public School buildSchool() {
        return new ModernSchoolBuilding();
    }

    @Override
    public House buildHouse() {
        return new ModernHouseBuilding();
    }
}
