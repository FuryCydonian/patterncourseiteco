package org.fury.abstractFactory;

public class ClassicBuildFactory implements BuildFactory {

    @Override
    public School buildSchool() {
        return new ClassicSchoolBuilding();
    }

    @Override
    public House buildHouse() {
        return new ClassicHouseBuilding();
    }
}
