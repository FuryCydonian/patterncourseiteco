package org.fury.abstractFactory;

public interface BuildFactory {
    School buildSchool();

    House buildHouse();
}
