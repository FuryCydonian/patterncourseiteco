package org.fury.singleton;

import java.io.*;

public final class RemoteConnector implements Externalizable {
    private static RemoteConnector instance;
    private boolean connected;

    private RemoteConnector() {}

    public synchronized void connect(String address) {
        if (connected) {
            return;
        }
        connected = true;
        // logic of connection
    }

    public static synchronized RemoteConnector getInstance() {
        if (instance == null) {
            instance = new RemoteConnector();
        }
        return instance;
    }

    @Override
    public void writeExternal(ObjectOutput out) throws IOException {
        throw new UnsupportedOperationException();
    }

    @Override
    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
        throw new UnsupportedOperationException();
    }
}
