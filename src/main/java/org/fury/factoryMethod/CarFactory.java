package org.fury.factoryMethod;

import java.time.LocalDateTime;

public class CarFactory {

    public static Car createCar(int weight, LocalDateTime creationTime, double engineCapacity) {
        Car car;
        if (weight > 100 && engineCapacity > 5) {
            return new JeepCar(weight, engineCapacity, LocalDateTime.now());
        } else {
            return new SedanCar(weight, engineCapacity, LocalDateTime.now());
        }
    }
}
