package org.fury.factoryMethod;

import java.time.LocalDateTime;

public class JeepCar implements Car {

    private final static int STANDARD_RADIUS_WHEEL = 300;

    private final int weight;
    private final double engineCapacity;
    LocalDateTime creationTime;

    public JeepCar(int weight, double engineCapacity, LocalDateTime creationTime) {
        this.weight = weight;
        this.engineCapacity = engineCapacity;
        this.creationTime = creationTime;
    }

    @Override
    public int getWeight() {
        return weight;
    }

    @Override
    public LocalDateTime GetDateCreation() {
        return creationTime;
    }

    @Override
    public double engineCapacity() {
        return engineCapacity;
    }

    public int getWheelsRadius() {
        return STANDARD_RADIUS_WHEEL;
    }
}
