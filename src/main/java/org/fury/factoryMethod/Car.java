package org.fury.factoryMethod;

import java.time.LocalDateTime;

public interface Car {
    int getWeight();

    LocalDateTime GetDateCreation();

    double engineCapacity();
}
