package org.fury.factoryMethod;

import java.time.LocalDateTime;

public class SedanCar implements Car {

    private final static int STANDARD_RADIUS_WHEEL = 200;

    private final int weight;
    private final double engineCapacity;
    LocalDateTime creationTime;

    public SedanCar(int weight, double engineCapacity, LocalDateTime creationTime) {
        this.weight = weight;
        this.engineCapacity = engineCapacity;
        this.creationTime = creationTime;
    }

    @Override
    public int getWeight() {
        return weight;
    }

    @Override
    public LocalDateTime GetDateCreation() {
        return creationTime;
    }

    @Override
    public double engineCapacity() {
        return engineCapacity;
    }

    public int getWheelRadius() {
        return STANDARD_RADIUS_WHEEL;
    }
}
