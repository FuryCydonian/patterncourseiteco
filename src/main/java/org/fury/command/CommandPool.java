package org.fury.command;

import java.util.List;

public class CommandPool extends Thread {
    private List<AircraftCommand> commands;

    public CommandPool(List<AircraftCommand> commands) {
        this.commands = commands;
    }

    @Override
    public void run() {
        while (true) {
            if (commands.size() == 0) {
                try {
                    sleep(10);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                AircraftCommand command = commands.remove(0);
                CommandExecutor executor = new CommandExecutor(command);
                executor.start();
            }
        }
    }
}
