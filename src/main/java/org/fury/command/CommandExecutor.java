package org.fury.command;

public class CommandExecutor extends Thread {
    private AircraftCommand command;

    public CommandExecutor(AircraftCommand command) {
        this.command = command;
    }

    @Override
    public void run() {
        try {
            sleep(1000L * command.getDelay());
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        //execute command
    }
}
