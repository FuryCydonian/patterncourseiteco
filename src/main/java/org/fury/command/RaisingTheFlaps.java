package org.fury.command;

public class RaisingTheFlaps implements AircraftCommand {
    private int timeAfterStart;
    private int angle;

    public RaisingTheFlaps(int timeAfterStart, int angle) {
        this.timeAfterStart = timeAfterStart;
        this.angle = angle;
    }

    @Override
    public int getDelay() {
        return timeAfterStart;
    }
}
