package org.fury.command;

public class StartEngine implements AircraftCommand {
    private int timeToStart;
    private int numberOfRevolutions;

    public StartEngine(int timeToStart, int numberOfRevolutions) {
        this.timeToStart = timeToStart;
        this.numberOfRevolutions = numberOfRevolutions;
    }

    @Override
    public int getDelay() {
        return timeToStart;
    }
}
