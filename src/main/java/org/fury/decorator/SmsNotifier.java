package org.fury.decorator;

public class SmsNotifier implements Notifier {

    private final MessengerNotifier messengerNotifier;

    public SmsNotifier(MessengerNotifier messengerNotifier) {
        this.messengerNotifier = messengerNotifier;
    }

    @Override
    public boolean notifySubscriber() {
        if (!messengerNotifier.notifySubscriber()) {
            String message = messengerNotifier.getText().substring(0, 160);
            //try to send message via sms
            return true;
        }
        return false;
    }
}
