package org.fury.decorator;

public interface Notifier {
    boolean notifySubscriber();
}
