package org.fury.decorator;

public final class MessengerNotifier implements Notifier {

    private final String phone;
    private final String text;

    public MessengerNotifier(String phone, String text) {
        this.phone = phone;
        this.text = text;
    }

    @Override
    public boolean notifySubscriber() {
        // trying to send via messenger
        return true;
    }

    public String getPhone() {
        return phone;
    }

    public String getText() {
        return text;
    }
}
