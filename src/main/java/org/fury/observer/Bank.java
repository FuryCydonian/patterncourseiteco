package org.fury.observer;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Collections;
import java.util.List;

public class Bank implements Observable {
    private List<Creditor> creditors = Collections.emptyList();

    @Override
    public void addObserver(Creditor creditor) {
        creditor.setAddedTime(LocalDateTime.now());
        creditors.add(creditor);
    }

    @Override
    public void removeObserver(int idNumber) {
        creditors.remove(idNumber);
    }

    @Override
    public void sendNotifications() {
        creditors.forEach(creditor -> {
            if (ChronoUnit.DAYS.between(LocalDateTime.now(), creditor.getAddedTime()) >= 30) {
                //send notification
                creditor.updateTime();
            }
        });
    }
}
