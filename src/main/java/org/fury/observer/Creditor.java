package org.fury.observer;

import java.time.LocalDateTime;

public class Creditor {
    private final int idNumber;
    private final String name;
    private LocalDateTime updatedTime;

    public Creditor(int idNumber, String name) {
        this.idNumber = idNumber;
        this.name = name;
    }

    public void updateTime() {
        updatedTime = LocalDateTime.now();
    }

    public int getIdNumber() {
        return idNumber;
    }

    public String getName() {
        return name;
    }

    public void setAddedTime(LocalDateTime addedTime) {
        this.updatedTime = addedTime;
    }

    public LocalDateTime getAddedTime() {
        return updatedTime;
    }
}
