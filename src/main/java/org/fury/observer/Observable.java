package org.fury.observer;

public interface Observable {
    void addObserver(Creditor observer);

    void removeObserver(int idNumber);

    void sendNotifications();
}
